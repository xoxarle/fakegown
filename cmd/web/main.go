package main

import (
	pagedata "fakegown/internal/data"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
)

type application struct {
	errorLog *log.Logger
	infoLog  *log.Logger
}

func (app *application) routes() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/", app.Page)
	mux.HandleFunc("/robots.txt", app.Robots)
	mux.HandleFunc("/words", app.Words)
	mux.HandleFunc("/phrase", app.Phrase)
	mux.HandleFunc("/paragraph", app.Para)
	mux.HandleFunc("/paragraphs", app.Paras)
	mux.HandleFunc("/markdown", app.Markdown)
	mux.HandleFunc("/markdown-post", app.MarkdownPost)
	mux.HandleFunc("/eleventy-post", app.MarkdownPost)
	mux.HandleFunc("/post", app.JsonPost)
	mux.HandleFunc("/page", app.Page)

	return app.logRequest(mux)
}

func main() {
	pagedata.InitalizeMarkov()
	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	addr := flag.String("addr", ":5201", "HTTP network address")
	flag.Parse()

	app := &application{
		errorLog: errorLog,
		infoLog:  infoLog,
	}

	fmt.Println("Starting fakegown....")

	srv := &http.Server{
		Addr:     *addr,
		ErrorLog: errorLog,
		// Call the new app.routes() method to get the servemux containing our routes.
		Handler: app.routes(),
	}

	infoLog.Printf("Starting server on %s", *addr)
	err := srv.ListenAndServe()
	errorLog.Fatal(err)
}
