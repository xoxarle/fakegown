package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"

	pagedata "fakegown/internal/data"
	"text/template"
)

type Post struct {
	Title  string `json:"title"`
	Author string `json:"author"`
	Date   string `json:"date"`
	Body   string `json:"body"`
}

var tplEnd = `
    </div>
    <footer>
    </footer>
</body>
</html>
`

var template_files = []string{
	"./ui/html/closing.tpl",
	"./ui/html/opening.tpl",
	"./ui/html/tplsec.tpl",
}

func (app *application) what_me_worry(w http.ResponseWriter, _ *http.Request) {
	w.Write([]byte("What? Me, worry?"))
}

func (app *application) Para(w http.ResponseWriter, r *http.Request) {
	res, err := pagedata.Md.Para(4+rand.Intn(7), true)
	if err != nil {
		w.Write([]byte("ERROR: " + err.Error()))
	} else {
		w.Write([]byte(res))

	}
}

func (app *application) Paras(w http.ResponseWriter, r *http.Request) {
	n_paras := 2 + rand.Intn(4)
	s_out := ""
	for h := 0; h < n_paras; h++ {
		res, err := pagedata.Md.Para(4+rand.Intn(7), true)
		if err != nil {
			w.Write([]byte("ERROR: " + err.Error()))
		} else {
			s_out = s_out + res + "\n\n"
		}
	}
	w.Write([]byte(s_out))
}

func (app *application) Phrase(w http.ResponseWriter, r *http.Request) {
	res, err := pagedata.Md.Phrase(9+rand.Intn(7), true)
	if err != nil {
		w.Write([]byte("ERROR: " + err.Error()))
	} else {
		w.Write([]byte(res))

	}
}

func (app *application) internalServerError(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte("internal server error"))
}

func (app *application) JsonPost(w http.ResponseWriter, r *http.Request) {
	var post Post
	post.Title, _ = pagedata.Md.Phrase(4+rand.Intn(4), false)
	post.Author, _ = pagedata.Md.Phrase(1+rand.Intn(2), false)

	var curDate = time.Now().AddDate(0, 0, 600-rand.Intn(1200))
	post.Date = curDate.Format("2006/01/02")
	post.Body, _ = pagedata.Md.Para(4+rand.Intn(4), true)
	w.Header().Set("content-type", "application/json")
	jsonBytes, err := json.Marshal(post)
	if err != nil {
		app.internalServerError(w, r)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(jsonBytes)
}

func (app *application) Words(w http.ResponseWriter, r *http.Request) {
	res, err := pagedata.Md.Phrase(4+rand.Intn(4), false)
	if err != nil {
		w.Write([]byte("ERROR: " + err.Error()))
	} else {
		w.Write([]byte(res))

	}
}

func (app *application) Markdown(w http.ResponseWriter, r *http.Request) {
	res, err := pagedata.Md.MarkdownBody()
	if err != nil {
		w.Write([]byte("ERROR: " + err.Error()))
	} else {
		w.Write([]byte(res))

	}
}

func (app *application) MarkdownPost(w http.ResponseWriter, r *http.Request) {
	res, err := pagedata.Md.MarkdownPost()
	if err != nil {
		w.Write([]byte("ERROR: " + err.Error()))
	} else {
		w.Write([]byte(res))

	}
}

func (app *application) Robots(w http.ResponseWriter, r *http.Request) {
	f, err := os.Open("./ui/html/robots.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	stats, statsErr := f.Stat()
	if statsErr != nil {
		w.Write([]byte(statsErr.Error()))
		return
	}

	var size int64 = stats.Size()
	bytes := make([]byte, size)

	bufr := bufio.NewReader(f)
	_, err = bufr.Read(bytes)
	if err != nil {
		w.Write([]byte(err.Error()))
		return
	} else {
		w.Write(bytes)
	}

}

func (app *application) Page(w http.ResponseWriter, r *http.Request) {

	if rand.Intn(6) == 1 {
		// una richiesta su 10 redirige su un bel filone da 10 gighelli.
		time.Sleep(7 * time.Second)
		http.Redirect(w, r, "https://ash-speed.hetzner.com/10GB.bin", http.StatusTemporaryRedirect)
	} else {
		i := 100
		pd := pagedata.MakePageData(i)

		//fmt.Printf("%+v\n", pd)

		tmpl, err := template.ParseFiles(template_files...)
		if err != nil {
			log.Print(err.Error())
			return
		}
		time.Sleep(2 * time.Second)

		var tpl bytes.Buffer

		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		tmpl.ExecuteTemplate(&tpl, "opening", *pd)

		w.Write(tpl.Bytes())

		if f, ok := w.(http.Flusher); ok {
			f.Flush()
		}

		time.Sleep(2 * time.Second)

		for i2 := 1; i2 < 8; i2++ {
			pd = pagedata.MakePageData(100 + i2)
			var tpl2 bytes.Buffer
			tmpl.ExecuteTemplate(&tpl2, "tplsec", *pd)
			w.Write(tpl2.Bytes())
			time.Sleep(1 * time.Second)

			if f, ok := w.(http.Flusher); ok {
				f.Flush()
			}
		}

		var tpl3 bytes.Buffer
		pd = pagedata.MakePageData(200)
		tmpl.ExecuteTemplate(&tpl3, "closing", *pd)
		w.Write(tpl3.Bytes())
		time.Sleep(2 * time.Second)

		if f, ok := w.(http.Flusher); ok {
			f.Flush()
		}
	}
}
