package main

import "net/http"

func (app *application) logRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		userIP := ""
		forwarded := r.Header.Get("X-Forwarded-For")
		if forwarded != "" {
			userIP = forwarded
		} else {
			userIP = r.RemoteAddr
		}
		userAgent := r.Header.Get("User-Agent")

		//app.infoLog.Printf("%s|%s|%s|%s|%s", r.Host, r.URL.Path, r.URL.RequestURI(), userIP, userAgent)
		app.infoLog.Printf("%s|%s|%s|%s|%s|%s", userIP, r.Proto, r.Method, r.URL.Path, r.URL.RequestURI(), userAgent)
		next.ServeHTTP(w, r)
	})
}
