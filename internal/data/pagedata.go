package pagedata

import (
	"math/rand"
	"strings"

	"codeberg.org/xoxarle/markov"
	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/html"
	"github.com/gomarkdown/markdown/parser"
)

var M *markov.Markov
var Md *markov.MarkovDown

type PageData struct {
	Title    string
	Subtitle string
	Body     string
	Links    map[string]string
	Cnt      int
}

func InitalizeMarkov() {
	//rand.Seed(time.Now().UnixNano())
	//m, _ := markov.InitMarkovFromTextFile("corpus.txt") //
	M := markov.CreateMarkov().LoadSampleData()
	Md = &markov.MarkovDown{Generator: M}
	M.MakeChain()
}

func MdToHTML(md []byte) string {
	// create markdown parser with extensions
	extensions := parser.CommonExtensions | parser.AutoHeadingIDs | parser.NoEmptyLineBeforeBlock
	p := parser.NewWithExtensions(extensions)
	doc := p.Parse(md)

	// create HTML renderer with extensions
	htmlFlags := html.CommonFlags | html.HrefTargetBlank
	opts := html.RendererOptions{Flags: htmlFlags}
	renderer := html.NewRenderer(opts)

	return string(markdown.Render(doc, renderer)[:])
}

func MakePageData(i int) *PageData {
	title, _ := Md.Phrase(3+rand.Intn(3), false)
	subtitle, _ := Md.Phrase(5+rand.Intn(6), false)
	body, _ := Md.Para(4+rand.Intn(4), true)
	body2 := MdToHTML([]byte(body))
	links := make(map[string]string)

	limit := 4 + rand.Intn(21)

	for i := 0; i < limit; i++ {
		link_text, _ := Md.Phrase(1+rand.Intn(4), false)
		link_url, _ := Md.Phrase(1+rand.Intn(4), false)
		link_url = strings.ReplaceAll(link_url, " ", "/")
		links[link_text] = "/moreinfo/" + link_url
	}

	pd := PageData{Title: title, Subtitle: subtitle, Body: body2, Links: links, Cnt: i}

	return &pd
}
