module fakegown

go 1.17

require (
	codeberg.org/xoxarle/markov v0.0.1
	github.com/gomarkdown/markdown v0.0.0-20241205020045-f7e15b2f3e62
)

require (
	github.com/gosimple/slug v1.12.0 // indirect
	github.com/gosimple/unidecode v1.0.1 // indirect
)
