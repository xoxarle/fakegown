{{ define "tplsec" }}
<section class="section_{{.Cnt}}">
  <h2>{{ .Title }}</h2>
  <h3>{{ .Subtitle }}</h3>
  <div class="sec-body">
  {{ .Body }}
  </div>
  <ul>
{{ range $key , $value  := .Links }}
                <li><a rel="nofollow" href="{{ $value}}">{{ $key }}</a> </li>
    {{ end }}

  </ul>
</section>
{{ end }}
