{{ define "opening" }}
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<title>{{ .Title }} {{ .Subtitle }}</title>
  <style>

  BODY {
  font-family: Avenir, Montserrat, Corbel, 'URW Gothic', source-sans-pro, sans-serif;
  font-size:14pt;
}

HEADER, ARTICLE, FOOTER{
  width:98%;
  max-width: 1154px;
  margin: 0 auto;
  padding: 1.5em;
}

HEADER {
  background-color: aliceblue;
}

FOOTER {
    color: white;
    background-color: navy;
  }

FOOTER A {
    color: yellow;

  }

ARTICLE {
  background-color:bisque;
}

section {
  background-color: skyblue;
  margin: 1em;
  padding: 1em;
}

HEADER { 
  text-align: center;
  font-size: 1.2em;
}

NAV { 
  text-align: center;
  line-height: 2.5em;
}
  
NAV A {
  padding: 0.5em;
  background-color: #069;
  color:white;
  font-weight: bold;
  border-radius: 0.5em;
  white-space: nowrap;
  
}

div.tabellonicus {
  display: grid;
  grid-template-columns: 1fr 1fr;
  width:100%;
  max-width: 1154px;
  margin: 0 auto;
}

  </style>
</head>
<body>
    <header>
        <h1>{{ .Title }}</h1>
        <h3>{{ .Subtitle }}</h3>
		<nav>
		{{ range $key, $value := .Links }}
		{{ $key }} | 
		{{ end }}
		</nav>
    </header>
    <article>
        {{ .Body }}
	</article>
  <div class='tabellonicus'>

{{ end }}
