{{ define "closing"}}
    </div>
    <footer>
    {{ .Body }}
	<nav>
<ul>
{{ range $key , $value  := .Links }}
                <li><a rel="nofollow" href="{{ $value}}">{{ $key }}</a> </li>
    {{ end }}

</ul>
	</nav>
    </footer>
</body>
</html>
{{ end }}
