# fakegown

A test web app for my Markov chain library (found in https://codeberg.org/xoxarle/markov) .  It is modelled upon https://github.com/maxlambertini/fakedown, a web app made using Python and Flask that does the same thing